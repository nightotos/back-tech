'use strict';
const User = require('../models/user.model');

//buscar todos los usuarios
exports.findAll = function (req, res) {
  User.findAll(function (err, user) {
    if (err)
      res.send(err);
    console.log(user);
    res.send(user);
  });
};

//crear usuario
exports.create = function (req, res) {
  const new_user = new User(req.body);
  //handles null error
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res.status(400).send({
      error: true,
      message: 'Please provide all required field'
    });
  } else {
    User.create(new_user, function (err, user) {
      if (err)
        res.send(err);
      res.json({
        error: false,
        message: "Usuario agregado correctamente!",
        data: user
      });
    });
  }
};

//buscar usuario por ID
exports.findById = function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

//actualizar usuario 
exports.update = function (req, res) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res.status(400).send({
      error: true,
      message: 'Please provide all required field'
    });
  } else {
    User.update(req.params.id, new User(req.body), function (err, user) {
      if (err)
        res.send(err);
      res.json({
        error: false,
        message: 'Usuario actualizado correctamente'
      });
    });
  }
};

//borrar usuario
exports.delete = function (req, res) {
  User.delete(req.params.id, function (err, user) {
    if (err)
      res.send(err);
    res.json({
      error: false,
      message: 'Empleado borrado correctamente'
    });
  });
};