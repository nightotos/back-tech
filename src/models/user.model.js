'use strict';
var dbConn = require('./../../config/db.config');
//user object create
var user = function (user) {
  this.ID_USUARIO = user.ID_USUARIO;
  this.ID_ROL = user.ID_ROL;
  this.NOMBRE = user.NOMBRE;
  this.ACTIVO = user.ACTIVO;
};
//guarda usuario
user.create = function (newUser, result) {
  dbConn.query("INSERT INTO usuario set ?", newUser, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res.insertId);
      result(null, res.insertId);
    }
  });
};
//buscar por id
user.findById = function (id, result) {
  dbConn.query("Select * from usuario where ID_USUARIO = ? ", id, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};
//buscar usuarios
user.findAll = function (result) {
  dbConn.query("Select * from usuario", function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      console.log('usuario : ', res);
      result(null, res);
    }
  });
};
//actualizar usuarios
user.update = function (id, user, result) {
  dbConn.query("UPDATE usuario SET ID_ROL=?,NOMBRE=?,ACTIVO=? WHERE ID_USUARIO = ?", [user.ID_ROL, user.NOMBRE, user.ACTIVO, id], function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};
//borrar usuario
user.delete = function (id, result) {
  dbConn.query("DELETE FROM usuario WHERE ID_USUARIO = ?", [id], function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};
module.exports = user;