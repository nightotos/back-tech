const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 5000;
const userRoutes = require('./src/routes/user.routes')

app.use(cors({
  origin: '*'
}))
app.use(express.json())
app.get('/', (req, res) => {
  res.send("Hello World");
});
app.use('/api/v1/users', userRoutes)
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});